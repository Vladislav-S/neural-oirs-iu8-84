import numpy as np
from neural import nueral

import kivy

kivy.require('1.11.1')

from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.widget import Widget
from kivy.properties import ListProperty, ObjectProperty
from kivy.graphics import Color, Ellipse, Line, Rectangle, Scale, Fbo
from kivy.core.window import Window

from kivy.config import Config

Config.set('graphics', 'resizable', '0')
Config.set('graphics', 'width', '500')
Config.set('graphics', 'height', '500')


class PaintBufferWidget(Widget):
    """
    Класс виджета для сжатого рисунка
    """
    lines = []

    def __init__(self, **kwargs):
        super(PaintBufferWidget, self).__init__(**kwargs)

    def export(self):
        self.export_to_png('export.png')

    def clear(self):
        del self.lines[:]
        self.canvas.clear()


class PaintWidget(Widget):
    """
    Класс виджета для рисования
    """
    lines = []
    resize_x = 80
    resize_y = 80

    def __init__(self, **kwargs):
        """
        Конструктор класса
        :param kwargs:
        """
        super(PaintWidget, self).__init__(**kwargs)
        self.paint_buffer = PaintBufferWidget()
        self.add_widget(self.paint_buffer, index=1)

    def clear(self):
        """
        Очистка холста
        :return:
        """
        del self.lines[:]
        self.canvas.clear()
        self.paint_buffer.clear()

    def on_touch_down(self, touch):
        """
        Ивент окна
        :param touch:
        :return:
        """
        if self.collide_point(*touch.pos):
            with self.canvas:
                self.lines.append(Line(pints=(touch.x, touch.y), width=10))
            with self.paint_buffer.canvas:
                self.paint_buffer.lines.append(Line(pints=(round(touch.x / self.resize_x),
                                                           round(touch.y / self.resize_y)), width=1))

    def on_touch_move(self, touch):
        """
        Ивент окна
        :param touch:
        :return:
        """
        if self.collide_point(*touch.pos):
            self.lines[len(self.lines) - 1].points += [touch.x, touch.y]
            self.paint_buffer.lines[len(self.paint_buffer.lines) - 1].points += [round(touch.x / self.resize_x),
                                                                                 round(touch.y / self.resize_y)]

    def on_touch_up(self, touch):
        """
        Ивент окна
        :param touch:
        :return:
        """
        if self.collide_point(*touch.pos):
            pass

    def btn_clear(self):
        """
        Обработчик нажатия кнопки очистки экрана
        :return:
        """
        self.clear()

    def btn_export(self):
        """
        Экспортируем изображения в файлы
        :return:
        """
        self.paint_buffer.export()
        self.export_to_png('export1.png')

    def get_img_array(self):
        """
        получаем пиксели картинки в виде линейного массива
        :return:
        """
        # TODO: return formatted pixel array
        img = self.paint_buffer.export_as_image()
        texture = img.texture
        region = texture.get_region(0, 0, 15, 15)

        temp_array = np.array([], dtype=int)

        for i in region.pixels[::4]:
            temp_array = np.append(temp_array, 0.01 if i < 0.5 else 0.99)

        return temp_array


class MainScreen(BoxLayout):
    label = ObjectProperty()
    state = "query"

    arrow = ['one', 'two', 'three', 'four']

    num_objects = 4  # left right top bottom | количество классов объектов
    num_iter = 3  # сколько раз рисуем каждый класс объектов при обучении
    num_iter_temp = 1  # служебная

    # temp = 0

    img_size = 15

    array = np.array([], int)
    result = np.array([], int)

    def __init__(self, **kwargs):
        """
        Конструктор класса
        :param kwargs:
        """
        super(MainScreen, self).__init__(**kwargs)
        Window.bind(on_key_down=self.on_keyboard_down)

        self.neural = nueral(i_nodes=225, h_nodes=40, o_nodes=4, learn_factor=0.4)

        try:
            # если у нас есть готовые тренировочные данные мы их используем
            self.array = np.load('train_img.npy')
            self.result = np.load('train_result.npy')
            self.train()
        except:
            pass

    def on_touch_up(self, touch):
        """
        Ивент корневого окна
        :param touch:
        :return:
        """
        # запускаем распознавание по завершению рисунка
        if touch.x < 1200 and touch.y < 1200:
            img = self.ids.p_w.get_img_array()
            result = self.neural.query(img)

            # TODO: вывод результата на основное окно
            print(result)

    def on_keyboard_down(self, keyboard, keycode, text, modifiers, etc):
        """
        Ивент корневого окна
        :param keyboard:
        :param keycode:
        :param text:
        :param modifiers:
        :param etc:
        :return:
        """
        if keycode == 32:  # SAPCE https://gist.github.com/Enteleform/a2e4daf9c302518bf31fcc2b35da4661

            if self.state == "train":

                # TODO: если нажат пробел - загружаем картинку в массив тренировочных картинок - готово
                # TODO: если счетчик экземпляров подходит к нулю - выходим из режима обучения - готово
                # TODO: прогоняем данные через сеть и входим в режим очереди - готово

                if self.num_iter_temp < self.num_objects * self.num_iter:

                    # сохранили новый тренировочный массив
                    if len(self.array) > 0:
                        self.array = np.vstack((self.array, self.ids.p_w.get_img_array()))
                    else:
                        self.array = np.hstack((self.array, self.ids.p_w.get_img_array()))

                    # значения правильного результата
                    self.result = np.append(self.result, self.num_iter_temp // self.num_iter)

                    self.label.text = 'print {0} / ({1})'.format(self.arrow[self.num_iter_temp
                                                                            // self.num_iter],
                                                                 self.num_iter_temp)

                    self.num_iter_temp += 1

                    self.ids.p_w.clear()

                # если мы закончили обучение
                elif self.num_iter_temp == self.num_objects * self.num_iter:
                    self.num_iter_temp = 1
                    self.label.text = 'done'

                    np.save('train_img.npy', self.array)
                    np.save('train_result.npy', self.result)

                    self.train()

                    self.state = "query"
                    self.ids.p_w.clear()

                pass

        return True

    def btn_train(self):
        """
        Обработчик события нажатия кнопки тренировки
        :return:
        """
        if self.state == "train" and self.num_iter_temp < self.num_iter * self.num_objects:
            self.state = "query"
            self.num_iter_temp = 1
            self.label.text = 'done'
            # np.save('train_img.npy', self.array)
            # np.save('train_result.npy', self.result)
        else:
            self.state = "train" if self.state == "query" else "query"
            self.label.text = 'print {0} / ({1})'.format(self.arrow[self.num_iter_temp
                                                                    // self.num_iter],
                                                         self.num_iter_temp)
            '''
            TODO: 
            1 - подгружать бинарник если такой есть
            2 - перехватывать режим 'cancel'
            '''

    def btn_additional(self):
        """
         Обработчик события нажатия дополнительной кнопки (test)
        :return:
        """
        self.train()

    def train(self):
        """
        Функция тренировки на основе загруженнх тренировочных данных
        :return:
        """
        i = 0
        while i < len(self.array):
            temp_res_arr = np.array([0., 0., 0., 0.], dtype=float)
            temp_res_arr[self.result[i]] = 0.99

            self.neural.train(self.array[i], temp_res_arr)

            i += 1


class MyApp(App):

    def build(self):
        return MainScreen()


if __name__ == '__main__':
    MyApp().run()
