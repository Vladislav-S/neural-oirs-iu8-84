import numpy as np
import scipy.special

class nueral():
    """Класс простой нейронной сети с тремя слоями с реализованным методом градиентного спуска
    :param i_nodes: Размер входного слоя
    :type i_nodes: int
    :param h_nodes: Размер скрытого слоя
    :type h_nodes: int
    :param o_nodes: Размер выходного слоя
    :type o_nodes: int
    :param learn_factor: Скорость обучение
    :type learn_factor: float
    """
    def __init__(self, i_nodes, h_nodes, o_nodes, learn_factor):
        """Конструктор
        """

        self.i_nodes = i_nodes
        self.h_nodes = h_nodes
        self.o_nodes = o_nodes

        self.learn_factor = learn_factor

        # self.w_i_h = (np.random.rand(self.h_nodes, self.i_nodes) - 0.5)
        # self.w_h_o = (np.random.rand(self.o_nodes, self.h_nodes) - 0.5)

        self.w_i_h = (np.random.normal(0.0, pow(self.h_nodes, -0.5),
                                       (self.h_nodes, self.i_nodes)))
        self.w_h_o = (np.random.normal(0.0, pow(self.o_nodes, -0.5),
                                       (self.o_nodes, self.h_nodes)))
        # функция активации - сигмоида
        self.activation_function = lambda x: scipy.special.expit(x)

    def train(self, input_list, target_list):
        """
        Функция тренировки
        :param input_list: Список входных значений в виде одномерного numpy массива
        :param target_list: Список значений в виде одномерного массива с правильным значением e.g: [0, 0, 1, 0]
        :return: void
        """
        inputs = np.array(input_list, ndmin=2).T
        targets = np.array(target_list, ndmin=2).T

        # входные и выходные значения скрытого слоя
        h_in = np.dot(self.w_i_h, inputs)
        h_out = self.activation_function(h_in)

        # входные и выходные значения выходного слоя
        final_in = np.dot(self.w_h_o, h_out)
        final_out = self.activation_function(final_in)

        # значение ошибки
        output_error = targets - final_out
        # значение ошибки скрытого слоя
        hidden_errors = np.dot(self.w_h_o.T, output_error)

        # метод градиентного спуска применимо от выходного к скрытому слоям
        self.w_h_o += self.learn_factor * np.dot((output_error
                                                  * final_out
                                                  * (1.0 - final_out))
                                                 , np.transpose(h_out))

        # метод градиентного спуска применимо от скрытого к выходному слоям
        self.w_i_h +=self.learn_factor * np.dot((hidden_errors
                                                 *h_out
                                                 *(1.0 - h_out))
                                                , np.transpose(inputs))


    def query(self, input_list):
        """
        Функция распознавания
        :param input_list: Список входных значений в виде одномерного numpy массива
        :return: Список значений в виде одномерного массива с правильным значением e.g: [0, 0, 1, 0]
        """
        inputs = np.array(input_list, ndmin=2).T

        h_in = np.dot(self.w_i_h, inputs)
        h_out = self.activation_function(h_in)

        final_in = np.dot(self.w_h_o, h_out)
        final_out = self.activation_function(final_in)

        return final_out
